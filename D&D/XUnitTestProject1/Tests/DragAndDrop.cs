using DragAndDropTest.Pages;
using Xunit;

namespace DragAndDropTest.Tests
{
    public class DragAndDrop : BaseTest
    {
        private DroppablePage DropPage { get; set; }
        private const string ExpectedColor = "rgba(255, 250, 144, 1)";
        private const string ExpectedText = "Dropped!";
        public DragAndDrop()
        {
            DropPage = new DroppablePage(Driver);
        }
        [Fact]
        public void DragAndDropTest()
        {
            DropPage.DragAndDropAction();
            Assert.Equal(ExpectedColor, DropPage.GetDroppableBackgroundColor());
            Assert.Equal(ExpectedText, DropPage.GetDroppableInnerText());
        }
    }
}
