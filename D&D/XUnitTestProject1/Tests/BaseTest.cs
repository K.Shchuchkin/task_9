﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;

namespace DragAndDropTest.Tests
{
    public abstract class BaseTest : IDisposable
    {
        protected IWebDriver Driver { get; private set; }
        public BaseTest()
        {
            Driver = new ChromeDriver(@$"{Environment.CurrentDirectory}\Resources");
            Driver.Manage().Timeouts().ImplicitWait = new TimeSpan(0, 0, 30);
            Driver.Manage().Window.Maximize();
            Driver.Navigate().GoToUrl("https://demoqa.com/droppable/");
        }
        public void Dispose()
        {
            Driver.Quit();
        }
    }
}
