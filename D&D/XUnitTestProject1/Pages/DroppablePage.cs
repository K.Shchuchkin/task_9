﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace DragAndDropTest.Pages
{
    public class DroppablePage : BasePage
    {
        private Actions Actions { get; set; }

        public DroppablePage(IWebDriver webDriver) : base(webDriver)
        {
            Actions = new Actions(webDriver);
        }

        #region WebElements
        public IWebElement Draggable => Driver.FindElement(By.Id("draggable"));
        public IWebElement Droppable => Driver.FindElement(By.Id("droppable"));
        public IWebElement TextInDroppable => Driver.FindElement(By.XPath("//p[parent::div[@id='droppable']]"));
        #endregion

        #region Actions
        public void DragAndDropAction() => Actions.DragAndDrop(Draggable, Droppable).Perform();
        public string GetDroppableBackgroundColor() => Droppable.GetCssValue("background-color");
        public string GetDroppableInnerText() => TextInDroppable.Text;
        #endregion
    }
}
