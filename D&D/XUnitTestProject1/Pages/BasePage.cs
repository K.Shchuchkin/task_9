﻿using OpenQA.Selenium;

namespace DragAndDropTest.Pages
{
    public abstract class BasePage
    {
        protected IWebDriver Driver { get; private set; }
        public BasePage(IWebDriver webDriver)
        {
            Driver = webDriver;
        }
    }
}
